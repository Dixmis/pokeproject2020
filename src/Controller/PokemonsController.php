<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Pokemons Controller
 *
 * @property \App\Model\Table\PokemonsTable $Pokemons
 * @method \App\Model\Entity\Pokemon[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PokemonsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'limit' => 30,
        ];

        $pokemons = $this->Pokemons->find('all')->contain(['PokemonStats.Stats', 'PokemonTypes.Types'])->order(["pokedex_number" => "ASC"]);
        $pokemons = $this->paginate($pokemons);

        $this->set(compact('pokemons'));
    }

    /**
     * View method
     *
     * @param string|null $id Pokemon id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pokemon = $this->Pokemons->get($id, [
            'contain' => ['PokemonStats.Stats', 'PokemonTypes.Types'],
        ]);
        
        $statsName = $this->getTableLocator()->get('stats')->find()->all()->combine("id","name")->toArray();
        $typeName = $this->getTableLocator()->get('types')->find()->all()->combine("id","name")->toArray();


        $this->set(compact('typeName'));
        $this->set(compact('statsName'));
        $this->set(compact('pokemon'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Pokemon id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pokemon = $this->Pokemons->get($id);
        if ($this->Pokemons->delete($pokemon)) {
            $this->Flash->success(__('The pokemon has been deleted.'));
        } else {
            $this->Flash->error(__('The pokemon could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function dashboard()
    {
        $pokemons = TableRegistry::getTableLocator()->get('pokemons');

        //Requete pour le poid moyen
        $weights = $pokemons->find();
        $weights->select(['poidsMoyen' => $weights->func()->avg('weight')])->where(['generation =' => 4]);

        $poidsMoyen = $weights->first();

        /**Recherche dans la base de donnée le nombre de pokémon de type fée par génération */
        function fairy($generation)
        {
            $pokemons = TableRegistry::getTableLocator()->get('pokemons');
            $fairy = $pokemons->find()->Join('pokemon_types')->Join('types');
            $fairy->select(['Nb_Type_Fee' => $fairy->func()->count('*')])
            ->where(['types.name =' => 'fairy'])
            ->andwhere(['pokemon_types.type_id = types.id'])
            ->andwhere(['pokemons.id = pokemon_types.pokemon_id'])
            ->andwhere(['pokemons.generation = ' => $generation]);

            return $fairy->first();
        }

        //choix des génération pour les types fée
        $generation = [1, 3, 7];
        $NombreTypeFee_1 = fairy($generation[0]);
        $NombreTypeFee_3 = fairy($generation[1]);
        $NombreTypeFee_7 = fairy($generation[2]);
        $NombreTypeFee = $NombreTypeFee_1->Nb_Type_Fee + $NombreTypeFee_3->Nb_Type_Fee + $NombreTypeFee_7->Nb_Type_Fee;

        //requete pour les 10 pokémons les plus rapide
        $speed = $pokemons->find()->Join('pokemon_stats')->Join('stats');
        $speed->select(['pokemon_stats.value', 'pokemons.name', 'pokemons.id', 'pokemons.default_front_sprite_url'])
        ->where(['stats.name =' => 'speed'])
        ->andwhere(['pokemon_stats.stat_id = stats.id'])
        ->andwhere(['pokemons.id = pokemon_stats.pokemon_id'])
        ->order(['pokemon_stats.value' => 'DESC'])
        ->limit(10);

        $speeder = $speed->all();

        $this->set(compact('poidsMoyen'));
        $this->set(compact('generation'));
        $this->set(compact('NombreTypeFee'));
        $this->set(compact('NombreTypeFee_1'));
        $this->set(compact('NombreTypeFee_3'));
        $this->set(compact('NombreTypeFee_7'));
        $this->set(compact('speeder'));
    }
}
