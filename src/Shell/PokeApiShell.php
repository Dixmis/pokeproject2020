<?php
declare(strict_types=1);

namespace App\Shell;

use Cake\Console\ConsoleOptionParser;
use Cake\Console\Shell;
use Cake\Http\Client;

/**
 * PokeApi shell command.
 */
class PokeApiShell extends Shell
{
    private $API_URL = "https://pokeapi.co/api/v2";
    private $time_start;
    private $time_end;

    /**
     * initialize function
     *
     * @return void
     */
    public function initialize(): void
    {
        $this->loadModel('Pokemons');
    }

    /**
     * main() method.
     *
     * @return void
     */
    public function main()
    {
        if (!isset($this->args[0]))
        $this->abort("Au moins un argument est requis\n'gen [num]' pour importer une génération en particulier\n'all' pour importer tout les pokémons");
        switch ($this->args[0]) {
            case 'gen':
            case 'generation':
                if (isset($this->args[1]) && is_numeric($this->args[1])) {
                    $time_start = time();
                    $this->_importGen($this->args[1]);
                } else $this->abort("Error with argument\n'gen [num]' should be the good format");
                break;
            case 'all':
                if ($count = $this->_countPokemons()) {
                    $time_start = time();
                    $this->out("Importing $count pokemons");
                    if ($this->_importAllPokemons($count))
                        $this->out("100% done!");
                    else
                        $this->abort("An error has occured");
                }
                break;
            default:
                $this->abort("Au moins un argument est requis\n'gen [num]' pour importer une génération en particulier\n'all' pour importer tout les pokémons");
        }
        $time_end = time();
        if (!empty($time_start))
            $this->out("Took " . ($time_end - $time_start) . " seconds");
    }

    protected function _countPokemons() {
        $http = new Client();

        $response = $http->get("$this->API_URL/pokemon");

        if ($response->isOk()) {
            $json = $response->getJson();

            return $json['count'];
        } else {
            $this->abort("Something wrong happened when calling PokeApi, aborting");
            return false;
        }
    }

    protected function _importAllPokemons($count) {

        $http = new Client();

        $response = $http->get("$this->API_URL/pokemon?limit=$count");

        if ($response->isOk()) {
            $json = $response->getJson();

            foreach ($json['results'] as $index => $pokemon) {
                $pokeApiData = $this->_getPokemonByURL($pokemon['url']);
                if (!$pokeApiData) continue;
                $pokedexNumber = $pokeApiData['pokedex_number'];
                if (!$this->Pokemons->exists(['pokedex_number' => $pokedexNumber])) {
                    $this->_createPokemon($pokeApiData);
                } else {
                    $this->verbose("The pokemon {$pokedexNumber} already exist in database");
                    $this->_updatePokemon($pokedexNumber, $pokeApiData);
                }
                if (($index % round($count/10)) == 0 && $index > 0) {
                    $pourcent = round($index / $count * 100);
                    $this->out("$pourcent% done!");
                }
            }
        } else {
            $this->verbose("Something wrong happened when calling PokeApi, aborting");
            return false;
        }
        return true;
    }

    protected function _importGen($gen) {
        $http = new Client();

        $response = $http->get("$this->API_URL/generation/$gen");

        if ($response->isOk()) {
            $json = $response->getJson();

            foreach ($json['pokemon_species'] as $specie) {
                $specie_response = $http->get($specie['url']);
                if ($specie_response->isOk()) {
                    foreach ($specie_response->getJson()['varieties'] as $varietie) {
                        $pokeApiData = $this->_getPokemonByURL($varietie['pokemon']['url']);

                        $pokedexNumber = $pokeApiData['pokedex_number'];
                        if (!$this->Pokemons->exists(['pokedex_number' => $pokedexNumber])) {
                            $this->_createPokemon($pokeApiData);
                        } else {
                            $this->verbose("The pokemon {$pokedexNumber} already exist in database");
                            $this->_updatePokemon($pokedexNumber, $pokeApiData);
                        }
                    }
                }
            }
        } else {
            $this->abort("Something wrong happened when calling PokeApi, aborting");
            return false;
        }
        return true;
    }

    /**
     * _getPokemonByURL function
     *
     * @param string $url pokemon API url
     * @return array
     */
    protected function _getPokemonByURL($url)
    {
        $http = new Client();

        $response = $http->get($url);

        if ($response->isOk()) {
            $pokemon = $response->getJson();
            $species_response = $http->get($pokemon['species']['url']);
            if ($species_response->isOk()) {
                $species = $species_response->getJson();
                switch ($species['generation']['name']) {
                    case 'generation-i':
                        $pokemon['generation'] = 1;
                        break;
                    case 'generation-ii':
                        $pokemon['generation'] = 2;
                        break;
                    case 'generation-iii':
                        $pokemon['generation'] = 3;
                        break;
                    case 'generation-iv':
                        $pokemon['generation'] = 4;
                        break;
                    case 'generation-v':
                        $pokemon['generation'] = 5;
                        break;
                    case 'generation-vi':
                        $pokemon['generation'] = 6;
                        break;
                    case 'generation-vii':
                        $pokemon['generation'] = 7;
                        break;
                    case 'generation-viii':
                        $pokemon['generation'] = 8;
                        break;
                    default:
                        $pokemon['generation'] = 0;
                }
            } else $pokemon['generation'] = 0;

            return $this->Pokemons->formatDataForSave($pokemon);
        } else {
            $number = substr($url, strlen("$this->API_URL/pokemon/"));
            $this->err("Something wrong happen during Api call with Pokemon id : {$number}");
            return false;
        }
    }

    /**
     * Undocumented function
     *
     * @param array $pokemonFormatedData formated data
     * @return void
     */
    protected function _createPokemon($pokemonFormatedData)
    {
        $pokemon = $this->Pokemons->newEntity($pokemonFormatedData);
        if (!$this->Pokemons->save($pokemon)) {
            $this->err('Something wrong happen');
            \Cake\Log\Log::write('error', json_encode($pokemon->getErrors()));
        }
    }

    /**
     * Undocumented function
     *
     * @param int $pokedexNumber pokedex number
     * @param array $pokemonFormatedData formated data
     * @return void
     */
    protected function _updatePokemon($pokedexNumber, $pokemonFormatedData)
    {
        $pokemon = $this->Pokemons->find()
                ->where(['pokedex_number' => $pokedexNumber])
                ->contain([
                    'PokemonStats.Stats',
                    'PokemonTypes.Types',
                ])
                ->first();

        $pokemon = $this->Pokemons->patchEntity($pokemon, $pokemonFormatedData);
        if (!$this->Pokemons->save($pokemon)) {
            $this->err('Something wrong happen');
            \Cake\Log\Log::write('error', json_encode($pokemon->getErrors()));
        }
    }
}
