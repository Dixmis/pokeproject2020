<br></br>
<h3>Poids Moyen</h3>
<table>
    <tr>
        <th>Génération</th>
        <th>Poids Moyen</th>
    </tr>
    <tr>
        <th>4</th>
        <td><?= h($poidsMoyen->poidsMoyen) ?></td>
    </tr>
</table>
<br></br>
<h3>Nombre de Pokémons de type Fée</h3>
<table>
    <tr>
        <th>Génération</th>
        <th>Nombre de type fée</th>
    </tr>
    
    <tr>
        <th><?= h($generation[0]) ?></th>
        <td><?= h($NombreTypeFee_1->Nb_Type_Fee) ?></td>
    </tr>
    <tr>
    <th><?= h($generation[1]) ?></th>
        <td><?= h($NombreTypeFee_3->Nb_Type_Fee) ?></td>
    </tr>
    <tr>
    <th><?= h($generation[2]) ?></th>
        <td><?= h($NombreTypeFee_7->Nb_Type_Fee) ?></td>
    </tr>
    <tr>
        <th>TOTAL</th>
        <th><?= h($NombreTypeFee) ?></th>
    </tr>
</table>
<br></br>
<h3>Les 10 pokémons les plus rapides</h3>
<table>
    <tr>
        <th>Pokémon</th>
        <th>Vitesse</th>       
    </tr>
    <?php foreach ($speeder as $pokemon) : ?>
    <tr>
        <td><?= $this->Html->image($pokemon->default_front_sprite_url, ["width" => 96, "height" => 96, "url" => ['action' => 'view', $pokemon->id]]); ?> <?= $this->Html->link(__($pokemon->name), ['action' => 'view', $pokemon->id]) ?></td>
        <td><?= h($pokemon->pokemon_stats['value']) ?></td>
    </tr>
    <?php endforeach; ?>
</table>