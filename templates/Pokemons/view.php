<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Pokemon $pokemon
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="pokemons view contentCARTE">
            <h3 class="carte"><?= h($pokemon->name) ?></h3>
            <div class="imgpokeport">
                    <?= $this->Html->image($pokemon->main_sprite, ["class" => "img-thumbnail"]); ?>
            </div>
            <div class="responsive">
                <?php if (!empty($pokemon->pokemon_types)) : ?>
                <div class="responsive">
                    <table>
                        <?php foreach ($pokemon->pokemon_types as $pokemonTypes) : ?>
                            <span class="responsive card--<?= h($typeName[$pokemonTypes->type_id]) ?>"><?= h($typeName[$pokemonTypes->type_id]) ?></span>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="related">
                <?php if (!empty($pokemon->pokemon_stats)) : ?>
                <div class="table-responsive">
                    <table>
                        <?php foreach ($pokemon->pokemon_stats as $pokemonStats) : ?>
                        <tr>
                            <td><?= h(preg_replace("/-/", " ", $statsName[$pokemonStats->stat_id])) ?></td>
                            <td><?= h($pokemonStats->value) ?></td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
            <div class="ca">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                    <?= $this->Html->image($pokemon->main_sprite, ["class" => "d-block w-100"]); ?>
                    </div>
                    <div class="carousel-item">
                    <?= $this->Html->image($pokemon->back_sprite, ["class" => "d-block w-100"]); ?>
                    </div>
                    <div class="carousel-item">
                    <?= $this->Html->image($pokemon->shiny_sprite, ["class" => "d-block w-100"]); ?>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <!-- <span class="sr-only">Previous</span> -->
                </a>
                <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <!-- <span class="sr-only">Next</span> -->
                </a>
            </div>
            <div>
            <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        </div>
    </div>
</div>
